<?php
declare(strict_types=1);

require_once (__DIR__ . '/vendor/autoload.php');

use App\ElevatorEngine;
use App\Elevator;
use App\Human;

$elevator = new Elevator(1, 1,10,700);

$elevatorEngine = new ElevatorEngine([$elevator]);

$human = new Human(80.0, 1);

$human->callElevator($elevatorEngine)
    ->enterElevator()
    ->pressFloor(2)
    ->leaveElevator();

