<?php
declare(strict_types=1);

namespace App;

class Elevator
{
    private $currentFloor = 0;
    private $maxFloor = 0;
    private $minFloor = 0;
    private $maxWeight = 0.0;
    private $currentWeight = 0.0;
    private $busy = false;
    private $overload = false;
    private $people = [];

    public function __construct(float $currentFloor, int $minFloor, int $maxFloor, float $maxWeight)
    {
        $this->currentFloor = $currentFloor;
        $this->minFloor = $minFloor;
        $this->maxFloor = $maxFloor;
        $this->maxWeight = $maxWeight;
    }

    public function enterHuman(Human $human)
    {
        $this->currentWeight += $human->getWeight();
        array_push($this->people, $human);
    }

    public function leaveHuman(Human $human)
    {
        $this->currentWeight -= $human->getWeight();
        $humanId = array_search($human, $this->people);
        unset($this->people[$humanId]);
    }

    public function moveTo(int $floor)
    {
        $this->busy = true;

        if ($this->isOverload()) {
            echo '*overload signal*' . PHP_EOL;
            return;
        }

        if (!$this->isInRange($floor)) {
            return;
        }

        if ($this->currentFloor < $floor) {
            $this->moveUp($floor);
        } else {
            $this->moveDown($floor);
        }

        echo 'Level: ' . $this->currentFloor . PHP_EOL;
        echo '*ding*' . PHP_EOL;
        $this->busy = false;
    }

    private function moveUp(int $floor)
    {
        while ($this->currentFloor < $floor) {
            echo 'Level: ' . $this->currentFloor . PHP_EOL;
            $this->currentFloor += 1;
            sleep(1);
        }
    }

    private function moveDown(int $floor)
    {
        while ($this->currentFloor > $floor) {
            echo 'Level: ' . $this->currentFloor . PHP_EOL;
            $this->currentFloor -= 1;
            sleep(1);
        }
    }

    public function isInRange(int $floor): bool
    {
        if ($floor > $this->maxFloor || $floor < $this->minFloor) {
            return false;
        } else {
            return true;
        }
    }

    public function isOverload(): bool
    {
        if ($this->maxWeight < $this->currentWeight) {
            $this->overload = true;
        } else {
            $this->overload = false;
        }

        return $this->overload;
    }

    public function isBusy(): bool
    {
        return $this->busy;
    }
}
