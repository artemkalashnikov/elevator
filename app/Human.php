<?php
declare(strict_types=1);

namespace App;

class Human
{
    private $weight = 0.0;
    private $currentFloor = 0;
    /** @var Elevator */
    private $elevator;

    public function __construct(float $weight, int $currentFloor)
    {
        $this->weight = $weight;
        $this->currentFloor = $currentFloor;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    public function callElevator(ElevatorEngine $elevatorEngine): Human
    {
        $this->elevator = $elevatorEngine->callElevator($this->currentFloor);

        return $this;
    }

    public function enterElevator(): Human
    {
        $this->elevator->enterHuman($this);
        echo 'Human enter' . PHP_EOL;

        return $this;
    }

    public function pressFloor(int $floor): Human
    {
        $this->elevator->moveTo($floor);

        return $this;
    }

    public function leaveElevator(): Human
    {
        $this->elevator->leaveHuman($this);
        echo 'Human leave' . PHP_EOL;

        return $this;
    }
}