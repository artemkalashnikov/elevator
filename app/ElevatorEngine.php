<?php
declare(strict_types=1);

namespace App;

use Exception;

class ElevatorEngine
{
    private $elevators = [];

    public function __construct(array $elevators)
    {
        $this->elevators = $elevators;
    }

    /**
     * @param int $floor
     * @return Elevator
     * @throws Exception
     */
    public function callElevator(int $floor): Elevator
    {
        $elevator = $this->findElevator($floor);
        $elevator->moveTo($floor);

        return $elevator;
    }

    /**
     * @param int $floor
     * @return Elevator
     * @throws Exception
     */
    private function findElevator(int $floor): Elevator
    {
        foreach ($this->elevators as $elevator) {
            if ($elevator->isBusy()) {
                continue;
            }

            if (!$elevator->isInRange($floor)) {
                continue;
            }

            return $elevator;
        }

        throw new Exception('No elevators in range');
    }
}